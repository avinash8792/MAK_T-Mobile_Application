package com.mak.client.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mak.soap.serviceimpl.Deal;
import com.mak.soap.serviceimpl.PromotionServiceImpl;
import com.mak.soap.serviceimpl.PromotionServiceImplService;

/**
 * Servlet implementation class DealsServlet
 */
@WebServlet("/DealsServlet")
public class DealsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PromotionServiceImpl webservice = new PromotionServiceImplService().getPromotionServiceImplPort();
		
		List<Deal> dealsList = webservice.getAllDeals();
		request.setAttribute("dealsList", dealsList);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/deals.jsp");
		requestDispatcher.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
