package com.mak.client.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.mbeans.UserMBean;

import com.mak.client.entity.User;
import com.mak.client.rest.UpdateUser;

/**
 * Servlet implementation class UpdateProfileServlet
 */
@WebServlet("/UpdateProfileServlet")
public class UpdateProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UpdateUser updateUser = new UpdateUser();
		
		User user = updateUser.getUserDetails((String)session.getAttribute("userName"));
		request.setAttribute("userDetails", user);
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/userprofile.jsp");
		requestDispatcher.forward(request, response);
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		UpdateUser updateUser = new UpdateUser();
		User user = new User();
		user.setFirstName(request.getParameter("firstName"));
		user.setLastName(request.getParameter("lastName"));
		user.setMobile(request.getParameter("mobile"));
		user.setUserName(request.getParameter("userName"));
		user.setPassword(request.getParameter("password"));
	
		boolean updated = updateUser.updateUserDetails(user);
		
		if(updated) {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/welcome.jsp");
			requestDispatcher.forward(request, response);
		}
		
		
	}

}
