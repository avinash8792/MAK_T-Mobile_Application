package com.mak.client.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mak.client.entity.User;
import com.mak.client.rest.AddUser;
import com.mak.client.rest.UserValidation;

/**
 * Servlet implementation class LoginServelt
 */
@WebServlet("/loginServelt")
public class LoginServelt extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		UserValidation userValidation = new UserValidation();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		boolean valid = userValidation.validateUser(username, password);
		if(valid) {
			session.setAttribute("userName",username );
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/ordercheckout.jsp");
			requestDispatcher.forward(request, response);
		}
		else {

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login_error.jsp");
			requestDispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		AddUser addUser = new AddUser();
		User newUser = new User();
		
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String userName = request.getParameter("userName");
		String mobile = request.getParameter("mobile");
		String password = request.getParameter("password");
		
		newUser.setFirstName(firstName);
		newUser.setLastName(lastName);
		newUser.setMobile(mobile);
		newUser.setUserName(userName);
		newUser.setPassword(password);
		
		boolean added = addUser.addAUser(newUser);
		if(added) {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login.jsp");
			requestDispatcher.forward(request, response);
		}
		else {
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/signup_error.jsp");
			requestDispatcher.forward(request, response);
			
		}
		
				
	}

}
