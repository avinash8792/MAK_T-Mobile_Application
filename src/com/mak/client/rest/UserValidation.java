package com.mak.client.rest;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class UserValidation {

	
	public boolean validateUser(String username, String password) {
		
	   Client client = ClientBuilder.newClient();
	  
	   WebTarget webTarget = client.target("http://localhost:8090/MAK_REST_SERVICE/User/validate");
	   Invocation.Builder invocationBuilder = webTarget
			                                  .queryParam("userName", username)
			                                  .queryParam("password", password)
			                                  .request();
	   Response response = invocationBuilder.get();  
	   boolean valid = response.readEntity(Boolean.class);
		return valid;
		
	}
}
