package com.mak.client.rest;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class DeleteUser {

	public boolean deleteUser(String userName) {
		
		
		
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8090/MAK_REST_SERVICE/User/deleteuser")
				                     .queryParam("userName", userName);
		Invocation.Builder invocationBuilder = webTarget.request();
		
		Response response = invocationBuilder.delete();
		
		boolean deleted = response.readEntity(Boolean.class);
		
		return deleted;
		
	}
	
}
