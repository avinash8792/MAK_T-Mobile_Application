package com.mak.client.rest;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mak.client.entity.User;

public class UpdateUser {

	
	public User getUserDetails(String userName) {
		
		 Client client = ClientBuilder.newClient();
		  
		   WebTarget webTarget = client.target("http://localhost:8090/MAK_REST_SERVICE/User/getUser");

		   Invocation.Builder invocationBuilder = webTarget
				                                  .queryParam("userName", userName)
				                                  .request();
		   Response response = invocationBuilder.get();  
		  
		   User user = response.readEntity(User.class);
		   
		  
			return user;
	}

	public boolean updateUserDetails(User user) {
		
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8090/MAK_REST_SERVICE/User/updateuser");
		Invocation.Builder invocationBuilder = webTarget
				                               .request();


		Response response = invocationBuilder.put(Entity.entity(user, MediaType.APPLICATION_JSON));
	
		String result = response.readEntity(String.class);
		boolean updated = Boolean.valueOf(result);
		
		
		return updated;
	}
}
