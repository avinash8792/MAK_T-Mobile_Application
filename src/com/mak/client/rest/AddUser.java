package com.mak.client.rest;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.catalina.mbeans.UserMBean;

import com.mak.client.entity.User;

public class AddUser {

	public boolean addAUser(User user) {
		
	
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8090/MAK_REST_SERVICE/User/adduser");	
		Response response1 = webTarget.request(MediaType.TEXT_PLAIN).post(Entity.entity(user, MediaType.APPLICATION_JSON));	
		boolean addedUser = response1.readEntity(Boolean.class);
		return addedUser;
		
	}
}
