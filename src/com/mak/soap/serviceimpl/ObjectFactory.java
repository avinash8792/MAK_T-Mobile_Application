
package com.mak.soap.serviceimpl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mak.soap.serviceimpl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Deal_QNAME = new QName("http://serviceImpl.soap.mak.com/", "deal");
    private final static QName _GetAllDealsResponse_QNAME = new QName("http://serviceImpl.soap.mak.com/", "getAllDealsResponse");
    private final static QName _GetAllDeals_QNAME = new QName("http://serviceImpl.soap.mak.com/", "getAllDeals");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mak.soap.serviceimpl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Deal }
     * 
     */
    public Deal createDeal() {
        return new Deal();
    }

    /**
     * Create an instance of {@link GetAllDeals }
     * 
     */
    public GetAllDeals createGetAllDeals() {
        return new GetAllDeals();
    }

    /**
     * Create an instance of {@link GetAllDealsResponse }
     * 
     */
    public GetAllDealsResponse createGetAllDealsResponse() {
        return new GetAllDealsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Deal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://serviceImpl.soap.mak.com/", name = "deal")
    public JAXBElement<Deal> createDeal(Deal value) {
        return new JAXBElement<Deal>(_Deal_QNAME, Deal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllDealsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://serviceImpl.soap.mak.com/", name = "getAllDealsResponse")
    public JAXBElement<GetAllDealsResponse> createGetAllDealsResponse(GetAllDealsResponse value) {
        return new JAXBElement<GetAllDealsResponse>(_GetAllDealsResponse_QNAME, GetAllDealsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllDeals }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://serviceImpl.soap.mak.com/", name = "getAllDeals")
    public JAXBElement<GetAllDeals> createGetAllDeals(GetAllDeals value) {
        return new JAXBElement<GetAllDeals>(_GetAllDeals_QNAME, GetAllDeals.class, null, value);
    }

}
