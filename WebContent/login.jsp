<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>


<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Login</title>

<!-- Bootstrap core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template -->
<link rel="stylesheet"
	href="vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet"
	href="vendor/simple-line-icons/css/simple-line-icons.css">
<link href="https://fonts.googleapis.com/css?family=Lato"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Muli"
	rel="stylesheet">

<!-- Plugin CSS -->
<link rel="stylesheet" href="device-mockups/device-mockups.min.css">

<!-- Custom styles for this template -->
<link href="css/new-age.min.css" rel="stylesheet">
<link rel="icon" href="img/T_Mobile.png">
</head>

<style>
.button {
	display: block;
	width: 100px;
	height: 50px;
	background: #D3D3D3;
	padding: 10px;
	text-align: center;
	border-radius: 5px;
	color: black;
	font-weight: bold;
}

.button1 {
	display: block;
	width: 200px;
	height: 60px;
	background: #D3D3D3;
	padding: 10px;
	text-align: center;
	border-radius: 5px;
	color: black;
	font-weight: bold;
}

.myclass {
	height: 20px;
	position: relative;
	border: 2px solid #cdcdcd;
	border-color: rgba(0, 0, 0, .14);
	background-color: AliceBlue;;
	font-size: 14px;
}
</style>

<body id="page-top">


	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-light fixed-top"
		id="mainNav">
	<div class="container">
		<a class="navbar-brand js-scroll-trigger" href="#page-top"></a>

		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<div id=over>
					<li class="nav-item"><img src="img/T_Mobile.png"
						class="img-fluid" alt="" style="width: 400px; height: 50px;">
						<p
							style="text-align: right; margin-top: 0px; margin-bottom: 0px; padding: 0px;">
						</p></li>
				</div>
				<li class="nav-item">
					<form action="DealsServlet">
						<input type="submit" class="button" value="Deals" />
					</form>
				</li>
			</ul>
		</div>
	</div>
	</nav>

	<header class="masthead">

	<div class="container h-100">
		<div class="row h-100">
			<div class="col-lg-7 my-auto">
				<div class="header-content mx-auto">
					<div class="col-sm-5" align="right">
						<h4 style="color: yellow">User Login</h4>
					</div>
					<br>

					<form action="loginServelt">
						<div class="row">
							<p>UserName</p>
							<div class="col-sm-5">
								<input type="text" name="username" class="myclass">
							</div>
						</div>
						<br>
						<div class="row">
							<p>Password</p>
							&nbsp;

							<div class="col-sm-5">
								<input type="password" name="password" class="myclass">
							</div>
						</div>
						<br>
						<div class="row" align="right">

							<div class="col-sm-5">

								<input type="submit" value="Login" class=button /> <br> <br>

								<div>
									New user? <a href="signup.jsp" class="">Sign_Up</a>
								</div>
							</div>
						</div>
					</form>


				</div>
			</div>
			<div class="col-lg-5 my-auto">
				<div class="device-container">
					<div class="device-mockup iphone6_plus portrait white">
						<div class="device">
							<div class="screen">
								<!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
								<img src="img/AppleWatch.png" class="img-fluid" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</header>

	<footer>
	<div class="container">

		<ul class="list-inline">
			<li class="list-inline-item"><a href="#">Privacy</a></li>
			<li class="list-inline-item"><a href="#">Terms</a></li>
			<li class="list-inline-item"><a href="#">FAQ</a></li>
		</ul>
	</div>
	</footer>

	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for this template -->
	<script src="js/new-age.min.js"></script>



</body>
</html>