<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Order Checkout</title>

<!-- Bootstrap core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template -->
<link rel="stylesheet"
	href="vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet"
	href="vendor/simple-line-icons/css/simple-line-icons.css">
<link href="https://fonts.googleapis.com/css?family=Lato"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Muli"
	rel="stylesheet">

<!-- Plugin CSS -->
<link rel="stylesheet" href="device-mockups/device-mockups.min.css">

<!-- Custom styles for this template -->
<link href="css/new-age.min.css" rel="stylesheet">

</head>

<style>
.button {
	display: block;
	width: 100px;
	height: 50px;
	background: #D3D3D3;
	padding: 10px;
	text-align: center;
	border-radius: 5px;
	color: black;
	font-weight: bold;
}

.button1 {
	display: block;
	width: 200px;
	height: 60px;
	background: #D3D3D3;
	padding: 10px;
	text-align: center;
	border-radius: 5px;
	color: black;
	font-weight: bold;
}
</style>

<body id="page-top">

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-light fixed-top"
		id="mainNav">
	<div class="container">
		<a class="navbar-brand js-scroll-trigger" href="#page-top"></a>

		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<p>
						<img src="img/T_Mobile.png" class="img-fluid" alt=""
							style="float: left; width: 400px; height: 50px;">
					</p>
				</li>
				<li class="nav-item"><b><a href="welcome.jsp"
						class="button" style="color: black"> Home</a></b></li> &nbsp; &nbsp;
				<li class="nav-item">

					<form action="DealsServlet">
						<input type="submit" class="button" value="Deals" />
					</form>
				</li> &nbsp; &nbsp;
				<li class="nav-item">

					<form action="LogoutServlet">

						<input type="submit" class="button" value="Logout" />
					</form>
				</li> &nbsp; &nbsp;
				<li class="nav-item">

					<form action="UpdateProfileServlet">

						<input type="submit" class="button" value="Profile" />
					</form>
				</li>
			</ul>
		</div>
	</div>
	</nav>

	<header class="masthead">
	<div class="container h-100">
		<div class="row h-100">
			<div class="col-lg-7 my-auto">
				<div class="header-content mx-auto">
					<p style="color: yellow">
						Hi!
						<c:out value="${sessionScope.userName}" />
					</p>
					<h1 style="color: yellow">Your order is ready for checkout!</h1>
					<div>
						<h4 style="color: yellow">Order Details:</h4>
					</div style="color:yellow">
					<p style="color: yellow">Apple 3 Series watch: 1000$</p>
					<p style="color: yellow">Total Cost: 1200$ (taxes included)</p>
					<br> <a href="ordersuccess.jsp" class="button" style="">
						Proceed</a>
				</div>

			</div>
			<div class="col-lg-5 my-auto">
				<div class="device-container">
					<div class="device-mockup iphone6_plus portrait white">
						<div class="device">
							<div class="screen">
								<!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
								<img src="img/AppleWatch.png" class="img-fluid" alt="">
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</header>

	<footer>
	<div class="container">

		<ul class="list-inline">
			<li class="list-inline-item"><a href="#">Privacy</a></li>
			<li class="list-inline-item"><a href="#">Terms</a></li>
			<li class="list-inline-item"><a href="#">FAQ</a></li>
		</ul>
	</div>
	</footer>

	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for this template -->
	<script src="js/new-age.min.js"></script>

</body>



</html>